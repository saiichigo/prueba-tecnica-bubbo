export const data = [
  {
    id: "9063b0b6-7ca4-4336-9458-3b1bb1f4c201",
    title: "One Hundred Years of Solitude",
    author: "Gabriel García Márquez",
    publicationYear: '1967-01-22',
    genre: "Magical Realism",
    description:
      "A famous novel that tells the story of the Buendía family over seven generations in the fictional town of Macondo.",
    images: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRZucj0mpfd4DmsEyKw1AS0a-DnE8bGUjRu_fN6g5iAoIfF1Vbk',
    pages: 260,
    cost: 3500,
    rate: 4.5
  },
  {
    id: "083d00c9-657c-4a4d-bd99-6478fefb2583",
    title: "1984",
    author: "George Orwell",
    publicationYear: '1949-01-22',

    genre: "Dystopian Fiction",
    description:
      "An influential book offering a grim view of a totalitarian future.",
    images: 'https://m.media-amazon.com/images/I/715WdnBHqYL._SY466_.jpg',
    pages: 370,
    cost: 3500,
    rate: 4.0

  },
  {
    id: "ea98e9e3-4ce4-482c-9b43-ee50410132e5",
    title: "The Lord of the Rings",
    author: "J.R.R. Tolkien",
    publicationYear: '1954-01-60',
    genre: "Fantasy",
    description:
      "An epic fantasy trilogy considered one of the greatest literary achievements of the 20th century.",
    images: 'https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1566425108i/33.jpg',
    pages: 420,
    cost: 8000,
    rate: 4.7
  },
];
