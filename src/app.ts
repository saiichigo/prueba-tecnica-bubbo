import express, { Request, Response } from 'express';
import multer from 'multer';
import admin from 'firebase-admin';
import { v4 as uuidv4 } from 'uuid';
import { data } from './config/data';
import { FirebaseConfig } from './config/appConfigFirebase';
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3000;
// Inicializar Firebase Admin
admin.initializeApp({
    credential: admin.credential.cert({
        clientEmail: FirebaseConfig.client_email,
        privateKey: FirebaseConfig.private_key,
        projectId: FirebaseConfig.project_id
    }),
    storageBucket: 'gs://bubbo-prueba-tecnica.appspot.com'
});

const app = express();
const bucket = admin.storage().bucket();

// app.use(express.json());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));


export const db = admin.firestore();

app.listen(PORT, () => console.log(`Servidor corriendo en el puerto ${PORT}`));



data.forEach(async (book: any) => {
    await db.collection('books').doc(book.id).set(book);
});


app.get('/', (req: Request, res: Response) => {
    res.send('Hello World!');
});

// GET /books
app.get('/books', async (req: Request, res: Response) => {
    const snapshot = await db.collection('books').get();
    const data = snapshot.docs.map(doc => doc.data());
    res.json(data);
});

// GET /books/{id}
app.get('/books/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const book = await db.collection('books').doc(id).get();
        if (!book.exists) {
            return res.status(404).send('Book not found');
        }
        res.json(book.data());
    } catch (error: any) {
        res.status(500).send(error.message);
    }
});
// POST /books
app.post('/books', async (req, res) => {
    const base64Image = req.body.book.images;
    const buffer = Buffer.from(base64Image, 'base64');
    const bookData = req.body.book;

    try {
        const id = uuidv4();
        const fileName = `${id}.png`;
        const blob = admin.storage().bucket().file(fileName);

        const blobStream = blob.createWriteStream({
            metadata: {
                contentType: 'image/png',
            },
        });

        blobStream.on('error', error => {
            console.error(error);
            return res.status(500).send('Unable to upload at the moment.');
        });

        blobStream.on('finish', async () => {
            const signedUrlConfig = { action: 'read', expires: '03-01-2500' };
            // @ts-ignore
            const [publicUrl] = await blob.getSignedUrl(signedUrlConfig);

            bookData.id = id;
            bookData.images = publicUrl;


            await admin.firestore().collection('books').doc(id).set(bookData);
            return res.status(201).send({ message: `Book added successfully`, url: publicUrl });
        });

        blobStream.end(buffer);
    } catch (error: any) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});




// PUT /books/{id}
app.put('/books/:id', async (req: Request, res: Response) => {
    const id = req.body.book.id;
    let bookData = req.body.book;

    try {

        if (bookData.images && bookData.images.startsWith('data:image/')) {
            const base64Image = bookData.images.split(';base64,').pop();
            const buffer = Buffer.from(base64Image, 'base64');
            const fileName = `${id}.png`; // o cualquier otra extensión de archivo
            const blob = admin.storage().bucket().file(fileName);
            const blobStream = blob.createWriteStream({
                metadata: {
                    contentType: 'image/png', // Asegúrate de poner el contentType correcto
                },
            });
            blobStream.on('error', error => {
                console.error(error);
                return res.status(500).send('Unable to upload at the moment.');
            });
            await new Promise((resolve, reject) => {
                blobStream.on('finish', () => {

                    const publicUrl = `https://storage.cloud.google.com/${bucket.name}/${fileName}`;
                    bookData.images = publicUrl;
                    resolve(publicUrl);
                });

                blobStream.end(buffer);
            });
        }
        await admin.firestore().collection('books').doc(id).update(bookData);
        res.status(200).send(`Book with ID: ${id} updated`);
    } catch (error: any) {
        console.error(error);
        res.status(500).send(error.message);
    }
});

// DELETE /books/{id}
app.delete('/books/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        await db.collection('books').doc(id).delete();
        res.status(200).send(`Book with ID: ${id} deleted`);
    } catch (error: any) {
        res.status(500).send(error.message);
    }
});