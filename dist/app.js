"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.db = void 0;
const express_1 = __importDefault(require("express"));
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const uuid_1 = require("uuid");
const data_1 = require("./config/data");
const appConfigFirebase_1 = require("./config/appConfigFirebase");
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000;
// Inicializar Firebase Admin
firebase_admin_1.default.initializeApp({
    credential: firebase_admin_1.default.credential.cert({
        clientEmail: appConfigFirebase_1.FirebaseConfig.client_email,
        privateKey: appConfigFirebase_1.FirebaseConfig.private_key,
        projectId: appConfigFirebase_1.FirebaseConfig.project_id
    }),
    storageBucket: 'gs://bubbo-prueba-tecnica.appspot.com'
});
const app = (0, express_1.default)();
const bucket = firebase_admin_1.default.storage().bucket();
// app.use(express.json());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
exports.db = firebase_admin_1.default.firestore();
app.listen(PORT, () => console.log(`Servidor corriendo en el puerto ${PORT}`));
data_1.data.forEach((book) => __awaiter(void 0, void 0, void 0, function* () {
    yield exports.db.collection('books').doc(book.id).set(book);
}));
app.get('/', (req, res) => {
    res.send('Hello World!');
});
// GET /books
app.get('/books', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const snapshot = yield exports.db.collection('books').get();
    const data = snapshot.docs.map(doc => doc.data());
    res.json(data);
}));
// GET /books/{id}
app.get('/books/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const book = yield exports.db.collection('books').doc(id).get();
        if (!book.exists) {
            return res.status(404).send('Book not found');
        }
        res.json(book.data());
    }
    catch (error) {
        res.status(500).send(error.message);
    }
}));
// POST /books
app.post('/books', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const base64Image = req.body.book.images;
    const buffer = Buffer.from(base64Image, 'base64');
    const bookData = req.body.book;
    try {
        const id = (0, uuid_1.v4)();
        const fileName = `${id}.png`;
        const blob = firebase_admin_1.default.storage().bucket().file(fileName);
        const blobStream = blob.createWriteStream({
            metadata: {
                contentType: 'image/png',
            },
        });
        blobStream.on('error', error => {
            console.error(error);
            return res.status(500).send('Unable to upload at the moment.');
        });
        blobStream.on('finish', () => __awaiter(void 0, void 0, void 0, function* () {
            const signedUrlConfig = { action: 'read', expires: '03-01-2500' };
            // @ts-ignore
            const [publicUrl] = yield blob.getSignedUrl(signedUrlConfig);
            bookData.id = id;
            bookData.images = publicUrl;
            yield firebase_admin_1.default.firestore().collection('books').doc(id).set(bookData);
            return res.status(201).send({ message: `Book added successfully`, url: publicUrl });
        }));
        blobStream.end(buffer);
    }
    catch (error) {
        console.error(error);
        return res.status(500).send(error.message);
    }
}));
// PUT /books/{id}
app.put('/books/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.body.book.id;
    let bookData = req.body.book;
    try {
        if (bookData.images && bookData.images.startsWith('data:image/')) {
            const base64Image = bookData.images.split(';base64,').pop();
            const buffer = Buffer.from(base64Image, 'base64');
            const fileName = `${id}.png`; // o cualquier otra extensión de archivo
            const blob = firebase_admin_1.default.storage().bucket().file(fileName);
            const blobStream = blob.createWriteStream({
                metadata: {
                    contentType: 'image/png', // Asegúrate de poner el contentType correcto
                },
            });
            blobStream.on('error', error => {
                console.error(error);
                return res.status(500).send('Unable to upload at the moment.');
            });
            yield new Promise((resolve, reject) => {
                blobStream.on('finish', () => {
                    const publicUrl = `https://storage.cloud.google.com/${bucket.name}/${fileName}`;
                    bookData.images = publicUrl;
                    resolve(publicUrl);
                });
                blobStream.end(buffer);
            });
        }
        yield firebase_admin_1.default.firestore().collection('books').doc(id).update(bookData);
        res.status(200).send(`Book with ID: ${id} updated`);
    }
    catch (error) {
        console.error(error);
        res.status(500).send(error.message);
    }
}));
// DELETE /books/{id}
app.delete('/books/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        yield exports.db.collection('books').doc(id).delete();
        res.status(200).send(`Book with ID: ${id} deleted`);
    }
    catch (error) {
        res.status(500).send(error.message);
    }
}));
